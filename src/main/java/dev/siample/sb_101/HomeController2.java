package dev.siample.sb_101;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController2 {
	
	SpyGirl spicy;
	
	@Autowired
	public void spyGirlSetter(SpyGirl spicy) {
		this.spicy = spicy;
	}
	
	@RequestMapping("/2")
	public String index() {
		return spicy.iSaySg() + " via @Component|@Service setter";
	}

}

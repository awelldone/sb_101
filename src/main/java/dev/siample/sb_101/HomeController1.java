package dev.siample.sb_101;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController1 {
	
	@Autowired
	SpyGirl spicy;
	
	@RequestMapping("/1")
	public String index() {
		return spicy.iSaySg() + " via @Autowired loosely coupled objectvariable.";
	}

}

package dev.siample.sb_101;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController3 {
	
	SpyGirl spicy;
	
	@Autowired
	public HomeController3(SpyGirl spicy) {
		this.spicy = spicy;
	}
	
	@RequestMapping("/3")
	public String index() {
		return spicy.iSaySg() + "%n via @(Rest)Controller setter";
	}

}
